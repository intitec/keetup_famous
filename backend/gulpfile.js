var gulp            = require('gulp');
var path            = require('path');
var sourcemaps      = require('gulp-sourcemaps');
var ts              = require('gulp-typescript');
var del             = require('del');
var rename          = require('gulp-rename');
var runSequence     = require('run-sequence');
var nodemon         = require('gulp-nodemon');
var colors          = require('colors');
var args            = require('yargs').argv;
var browserSync     = require('browser-sync').create();
var reload          = browserSync.reload;

var now             = (new Date()).getTime();
var production      = (args.env == 'production') ? true : false;


/* ====================================================================== */

gulp.task('clean', function(){
    console.log(' + Clean '.white.bgGreen);

    return del('dist');
});

/* ====================================================================== */

gulp.task('environment', function(){
    console.log(' + Environment '.white.bgGreen);

    var r = [];

    if(production){
        r.push(del('dist/environment.js'));
        gulp.src('dist/environment.prod.js').pipe(rename('environment.js')).pipe(gulp.dest('dist'));
        r.push(del('dist/environment.prod.js'));
    }
    else{
        r.push(del('dist/environment.prod.js'));
    }

    return r;
});

/* ====================================================================== */

gulp.task('server:build', function(){
    console.log(' + Compiling server '.white.bgGreen);
    return gulp.src(['src/**/*.ts']).pipe(ts({module: 'commonjs'})).js.pipe(gulp.dest('dist'));
});


gulp.task('server:nodemon', ['server:build'], function(cb){
    console.log(' - Nodemon '.white.bgYellow);

    var started = false;
    nodemon({
        script: 'dist/server.js'
    })
    .on('start', function(){
        console.log(' · Nodemon started server.js '.white.bgCyan);

        if(!started) cb();
        started = true;
    })
    .on('restart', function(){
        console.log(' · Nodemon restarted server.js '.white.bgCyan);
    })
});

gulp.task('server:browser-sync', ['server:nodemon'], function(){
    console.log(' - BrowserSync '.white.bgYellow);

    browserSync.init({
        proxy: "http://localhost:4200"
    });
});

gulp.task('server:watch', function(){
    console.log(' - Watch '.white.bgYellow);

    gulp.watch('./src/**/*.ts', ['server:build', reload]);
});

/* ===== INIT
======================================================================= */

gulp.task('build', function(callback){
    if(production){
        console.log(' >>> Production build <<< '.white.bgRed);

        runSequence(
            'clean',
            'server:build',
            'environment',
            callback
        );
    }
    else{
        console.log(' >>> Dev build <<< '.white.bgRed);
        runSequence(
            'clean',
            'server:browser-sync',
            'environment',
            'server:watch',
            callback
        );
    }
});

gulp.task('default', ['build']);
