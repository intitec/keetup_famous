import {writeFileSync} from 'fs';
let http = require('http');

export function saveBase64ToFile(_base64:string, pathFile:string){
    let base64 = _base64.replace(/^data:image\/[a-z]+;base64,/, '');
    let bitmap = new Buffer(base64, 'base64');
    writeFileSync(pathFile, bitmap);
}
