import * as jwt from 'jsonwebtoken';

export function authorization(req:any, res:any){
    if(req.headers && req.headers.authorization){
        let parts = req.headers.authorization.split(' ');
        let result = jwt.decode(parts[1]);

        if(!result){
            res.status(400);
            res.send('token_invalid');
            return false;
        }

        req.user = result;
        return true;
    }
    else{
        res.status(400);
        res.send('token_not_provided');
        return false;
    }
}
