export function toReadable(d:any):string{
    let date:any =  new Date(d);
    let minutes = (date.getMinutes() < 10) ? ('0' + date.getMinutes()) : date.getMinutes();
    let day = (date.getDate() < 10) ? ('0' + date.getDate()) : date.getDate();
    let month = date.getMonth()+1;
    if(month < 10) month = '0' + month;
    let year = date.getFullYear().toString().substr(2,2);

    return `${day}/${month}/${year} ${date.getHours()}:${minutes}`;
}
