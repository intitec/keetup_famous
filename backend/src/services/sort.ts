export function sortObj(obj, order) {
    let key;
    let tempArry = [];
    let tempObj = {};

    for(key in obj)
        tempArry.push(key);

    tempArry.sort(
        (a, b) => {
            return a.toLowerCase().localeCompare(b.toLowerCase());
        }
    );

    if(order === 'desc')
        for(let i = tempArry.length - 1; i >= 0; i--)
            tempObj[tempArry[i]] = obj[tempArry[i]];
    else
        for(let i = 0; i < tempArry.length; i++)
            tempObj[tempArry[i]] = obj[tempArry[i]];

    return tempObj;
};