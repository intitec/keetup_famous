import * as express from 'express';
import * as http from 'http';
import * as mongoose from 'mongoose';
import * as bluebird from 'bluebird';
import {json, urlencoded} from 'body-parser';

import {ENV} from './environment';

import {FamousGuysController} from './controllers/famous-guys';

class App{
  private app:any;
  private server:any;
  private http:any;
  private famousGuysCtrl;

  constructor(){
    this.famousGuysCtrl = new FamousGuysController();
    this.app = express();
    this.http = new http.Server(this.app);

    this.connectDB();
    this.configure();
    this.routes();
    this.initServer();
  }

  private connectDB(){
    mongoose.Promise = bluebird.Promise;
    mongoose.connect(ENV.connectionString, { useMongoClient: true });
  }

  private configure():void{
      this.app.set('port', ENV.port.http);
      this.app.use(json({limit: '5mb'}));
      this.app.use(urlencoded({extended: false}));
  }

  private routes(){
    this.app.delete('/famous-guys/:id', this.famousGuysCtrl.remove);
    this.app.get('/famous-guys', this.famousGuysCtrl.list);
    this.app.put('/famous-guys', this.famousGuysCtrl.edit);
    this.app.post('/famous-guys', this.famousGuysCtrl.create);
    this.app.post('/famous-guys/localize', this.famousGuysCtrl.localize);
  }

  private initServer(){
      this.server = this.http.listen(ENV.port.http, () => {
          console.log(`Express server listening on port ${this.app.get('port')}`);
      });
  }
}

export default new App();
