import {Document, Schema, model} from "mongoose";
import {FamousGuysInterface} from "../interfaces/famous-guy";

interface FamousGuysModel extends FamousGuysInterface, Document{};

let FamousGuysSchema = new Schema({
  "name": {type: String},
  "description": {type: String}
});

let FamousGuy = model<FamousGuysModel>('famousGuys', FamousGuysSchema, 'famousGuys');

export = FamousGuy;
