import * as FamousGuy from '../models/famous-guy';
import * as https from 'https';
import * as request from 'request';
import {ENV} from '../environment';
import {FamousGuyData} from '../types/famous-guy';

export class FamousGuysController{

  constructor(){

  }

  list(req, res):void{
    FamousGuy.find(
      {},
      (error, data) => {
        if(error)
          res.json({ code: 500 });
        else
          res.json({ code: 200, data: data });
      }
    );
  }

  create(req, res):void{
    let item:FamousGuyData = req.body;

    FamousGuy.create(
      item,
      (error, data) => {
        if(error)
          res.json({ code: 500 });
        else
          res.json({ code: 200, data: data });
      }
    );
  }

  edit(req, res):void{
    let id = req.body._id;
    let item:FamousGuyData = req.body;

    FamousGuy.update(
      {_id: id},
      item,
      (error) => {
        if(error)
          res.json({ code: 500 });
        else
          res.json({ code: 200 });
      }
    );
  }

  remove(req, res):void{
    let id:string = req.params.id;

    FamousGuy.findByIdAndRemove(
      id,
      (error) => {
        if(error)
          res.json({ code: 500 });
        else
          res.json({ code: 200 });
      }
    );
  }

  localize(req, res):void{
    let latLng = req.body;

    let r = 300/111300; // = 300 meters
    let u = Math.random();
    let v = Math.random();
    let w = r * Math.sqrt(u);
    let t = 2 * Math.PI * v;
    let x = w * Math.cos(t);
    let y1 = w * Math.sin(t);
    let x1 = x / Math.cos(latLng.lat);

    let newY = latLng.lat + y1;
    let newX = latLng.lng + x1;

    let url = `https://maps.googleapis.com/maps/api/directions/json?origin=${latLng.lat},${latLng.lng}&destination=${newY},${newX}&mode=walking&key=${ENV.map.apiKey}`;

    let getRandomFamous = (latLng) => {
      FamousGuy.count().exec((err, count) => {
        let random = Math.floor(Math.random() * count);

        FamousGuy.findOne()
        .skip(random)
        .exec((error, data) => {

          if(error)
            res.json({ code: 500 });
          else
            res.json({
              code: 200,
              data: {
                item: data,
                latLng: latLng
              }
            });

        });
      });
    };

    request({url: url, json: true}, (err, _res, json) => {
      if(err)
        res.json({ code: 500 });
      else
        getRandomFamous(json.routes[0].legs[0].end_location);
    });
  }

};
