export interface FamousGuysInterface{
    id?: string;
    name: string;
    description: string;
}
