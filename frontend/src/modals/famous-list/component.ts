import { Component, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ModalFamousCreateEdit } from '../famous-create-edit/component';
import { ModalFamousRemove } from '../famous-remove/component';
import { LocalStorageService } from '../../services/localstorage';
import { FamousGuyData } from '../../types/famous-guy';
import { FamousGuysProvider } from '../../services/famous-guys';

@Component({
  selector: 'modal-famous-list',
  templateUrl: 'view.html',
})
export class ModalFamousList {
  items:any = null;

  constructor(
    public dialog: MatDialog,
    public modalRef: MatDialogRef<ModalFamousList>,
    private localStorageSvc:LocalStorageService,
    private famousGuysPvd:FamousGuysProvider
  ){
    this.getList();
  }

  private getList(){
    this.famousGuysPvd.list().subscribe(
      (data:any) => {
        if(data.code == 200)
          this.items = data.data;
      },
      error => console.error(error)
    );
  }

  onOk():void {
    this.modalRef.close();
  }

  onRemove(position){
    const dialogRef = this.dialog.open(ModalFamousRemove, {
      width: '560px'
    });

    dialogRef.afterClosed().subscribe((flag:boolean) => {
      if(flag){
        this.famousGuysPvd.remove(this.items[position]._id).subscribe(
          (data:any) => {
            if(data.code == 200)
              this.items.splice(position, 1);
          },
          error => console.error(error)
        );
      }
    });
  }

  onEdit(position){
    let item = JSON.parse(JSON.stringify(this.items[position]));

    const dialogRef = this.dialog.open(ModalFamousCreateEdit, {
      width: '560px',
      data: this.items[position]
    });

    dialogRef.afterClosed().subscribe((data:FamousGuyData) => {
      if(data){
        this.famousGuysPvd.update(data).subscribe(
          (data:any) => {
            if(data.code == 500){
              this.items = null;
              this.getList();
            }
          },
          error => console.error(error)
        );
      }
      else{
        this.items[position] = item;
      }
    });
  }

  onCreate(){
    const dialogRef = this.dialog.open(ModalFamousCreateEdit, {
      width: '560px',
      data: {name: '', description: ''}
    });

    dialogRef.afterClosed().subscribe((data:FamousGuyData) => {
      if(data){
        this.famousGuysPvd.add(data).subscribe(
          (data:any) => {
            if(data.code == 200)
              this.items.push(data.data);
          },
          error => console.error(error)
        );
      }
    });
  }

}
