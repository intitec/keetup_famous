import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'modal-famous-remove',
  templateUrl: 'view.html'
})
export class ModalFamousRemove {

  constructor(
    public modalRef: MatDialogRef<ModalFamousRemove>
  ){

  }

  onCancel(){
    this.modalRef.close();
  }

}
