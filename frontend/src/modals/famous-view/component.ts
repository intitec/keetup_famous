import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FamousGuyData } from '../../types/famous-guy';

@Component({
  selector: 'modal-famous-view',
  templateUrl: 'view.html'
})
export class ModalFamousView {

  constructor(
    public modalRef: MatDialogRef<ModalFamousView>,
    @Inject(MAT_DIALOG_DATA) public data:FamousGuyData
  ){

  }

  onOk(){
    this.modalRef.close();
  }

}
