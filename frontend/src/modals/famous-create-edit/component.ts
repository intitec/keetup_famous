import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FamousGuyData } from '../../types/famous-guy';

@Component({
  selector: 'modal-famous-create-edit',
  templateUrl: 'view.html',
  styleUrls: ['style.scss']
})
export class ModalFamousCreateEdit {
  addFlag:boolean;

  constructor(
    public modalRef: MatDialogRef<ModalFamousCreateEdit>,
    @Inject(MAT_DIALOG_DATA) public data:FamousGuyData
  ){
    this.addFlag = this.data.name == '';
  }

  onCancel(){
    this.modalRef.close();
  }

}
