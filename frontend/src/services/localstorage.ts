import {Injectable} from '@angular/core';

@Injectable()
export class LocalStorageService{

  public set(key:string, value:any):void{
    window.localStorage[key] = value;
  };

  public get(key:string, defaultValue = null):any{
    return window.localStorage[key] || defaultValue;
  };

  public setObject(key:string, value:any):void{
    window.localStorage[key] = JSON.stringify(value);
  };

  public getObject(key:string):any{
    return JSON.parse(window.localStorage[key] || '{}');
  };

  public getArray(key:string):any{
    return JSON.parse(window.localStorage[key] || '[]');
  };

  public pushInArray(key:string, value:any){
    let arr = JSON.parse(window.localStorage[key] || '[]');
    arr.push(value);
    window.localStorage[key] = JSON.stringify(arr);
  }

  public removeItemInArray(key:string, position:number){
    let arr = JSON.parse(window.localStorage[key] || '[]');
    arr.splice(position, 1);
    window.localStorage[key] = JSON.stringify(arr);
  }

  public editItemInArray(key:string, position:number, value:any){
    let arr = JSON.parse(window.localStorage[key] || '[]');
    arr[position] = value;
    window.localStorage[key] = JSON.stringify(arr);
  }

  public remove(key:string):void{
    window.localStorage.removeItem(key);
  };

  public exist(key:string):any{
    return (window.localStorage[key]) ? true : false;
  };
}
