import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable()
export class FamousGuysProvider{

  constructor(
    private httpClient: HttpClient
  ){

  }

  localize(data){
    let url = `${environment.server}/famous-guys/localize`;
    return this.httpClient.post(url, data);
  }

  list(){
    let url = `${environment.server}/famous-guys`;
    return this.httpClient.get(url);
  }

  update(data){
    let url = `${environment.server}/famous-guys`;
    return this.httpClient.put(url, data);
  }

  remove(id){
    let url = `${environment.server}/famous-guys/${id}`;
    return this.httpClient.delete(url);
  }

  add(data){
    let url = `${environment.server}/famous-guys`;
    return this.httpClient.post(url, data);
  }
}
