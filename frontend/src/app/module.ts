import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { environment } from '../environments/environment';

import { AppComponent } from './component';
import { ModalFamousList } from '../modals/famous-list/component';
import { ModalFamousCreateEdit } from '../modals/famous-create-edit/component';
import { ModalFamousRemove } from '../modals/famous-remove/component';
import { ModalFamousView } from '../modals/famous-view/component';

import { LocalStorageService } from '../services/localstorage';
import { FamousGuysProvider } from '../services/famous-guys';

import {
    MatCheckboxModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatMenuModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatTabsModule,
    MatButtonModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    MatTableModule,
    MatSortModule,
    MatAutocompleteModule,
    MatExpansionModule
} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    ModalFamousList,
    ModalFamousCreateEdit,
    ModalFamousRemove,
    ModalFamousView
  ],
  entryComponents: [
    ModalFamousList,
    ModalFamousCreateEdit,
    ModalFamousRemove,
    ModalFamousView
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    NoopAnimationsModule,
    AgmCoreModule.forRoot({
      apiKey: environment.map.apiKey
    }),
    MatCheckboxModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatMenuModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatTabsModule,
    MatButtonModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    MatTableModule,
    MatSortModule,
    MatAutocompleteModule,
    MatExpansionModule
  ],
  providers: [
    LocalStorageService,
    FamousGuysProvider
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
