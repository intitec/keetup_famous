import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ModalFamousList } from '../modals/famous-list/component';
import { ModalFamousView } from '../modals/famous-view/component';
import { environment } from '../environments/environment';
import { FamousGuysProvider } from '../services/famous-guys';

@Component({
  selector: 'app-root',
  templateUrl: 'view.html',
  styleUrls: ['style.scss']
})
export class AppComponent {
  ENV = environment;
  currentPosition:{lat:number, lng:number};
  famousGuy;

  constructor(
    public dialog: MatDialog,
    private famousGuysPvd:FamousGuysProvider
  ){
    this.setCurrentPosition();
  }

  private getRandomPoint(){
    this.famousGuysPvd
    .localize(this.currentPosition)
    .subscribe(
      (data:any) => {
        if(data.code == 200)
          this.famousGuy = data.data;
      },
      error => console.error(error)
    );
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.currentPosition = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        this.getRandomPoint();
      });
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalFamousList, {
      width: '500px'
    });
  }

  markerClicked(data){
    const dialogRef = this.dialog.open(ModalFamousView, {
      width: '500px',
      data: data.item
    });
  }
}
