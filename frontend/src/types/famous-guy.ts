export interface FamousGuyData {
  _id?: string;
  name: string;
  description: string;
}
